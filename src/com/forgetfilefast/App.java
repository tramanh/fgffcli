package com.forgetfilefast;

import com.sun.net.httpserver.HttpServer;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class App {
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JTextField serverPort;
    private JTextField indexURL;
    private JTextField storageFolder;
    private JButton browseButton;
    private JTextField searchBox;
    private JButton searchButton;
    private JTable table1;
    private JTextField textField5;
    private JButton SaveButton;
    private JButton cancelButton;
    private JButton updateButton;
    private JList list1;
    private JButton clearLogsButton;
    private JTable localTable;
    private JButton startButton;
    private JTextField interval;
    private JButton refreshButton;
    private JLabel result1;
    private JButton downloadButton;
    private HttpServer httpserver;
    private ScheduledExecutorService executor;
    public App() {


        Properties prop = new Properties();
        InputStream input = null;
        File f = new File("app.properties");
        if (f.isFile() && f.canRead()) {
            try {

                input = new FileInputStream("app.properties");
                prop.load(input);
                storageFolder.setText(prop.getProperty("storageFolder"));
                serverPort.setText(prop.getProperty("serverPort"));
                indexURL.setText(prop.getProperty("indexURL"));
                interval.setText(prop.getProperty("interval"));
            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        list1.setModel(new DefaultListModel());
        // table1
        DefaultTableModel table1model = new DefaultTableModel();
        table1.setModel(table1model);
        table1model.addColumn("Filename");
        table1model.addColumn("SHA256");
        table1.setDefaultEditor(Object.class, null);
        // table2

        DefaultTableModel table3model = new DefaultTableModel();
        localTable.setModel(table3model);
        table3model.addColumn("Filename");
        table3model.addColumn("SHA256");
        localTable.setDefaultEditor(Object.class, null);

        updateFolder();

        browseButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
                jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int returnValue = jfc.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = jfc.getSelectedFile();
                    storageFolder.setText(selectedFile.getAbsolutePath());
                }
            }
        });

        updateButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                // trigger update
//                DefaultListModel dlm =
//                        (DefaultListModel) list1.getModel();
//                dlm.addElement
//                        ("Update pressed");

                // update, get file list
                // export from current model
                updateFolder();
                DefaultTableModel model = (DefaultTableModel) localTable.getModel();
                int rowCount = model.getRowCount();
                String[] result = new String[rowCount];

                for (int i = 0; i < rowCount; i++) {
                    result[i] = (String)localTable.getValueAt(i, 0) + "/" + localTable.getValueAt(i, 1);
                }

                // push server

                if (startButton.getText().equalsIgnoreCase("start")) // not running
                {
                    JOptionPane.showMessageDialog(null, "Local server isn't running");
                    return;
                }
                try{
                    System.out.println(indexURL.getText() + "update?" + serverPort.getText() );
                    URL obj = new URL(indexURL.getText()   + "update?" + serverPort.getText() );

                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    OutputStreamWriter out = new OutputStreamWriter(
                            con.getOutputStream());
                    out.write(String.join("\n", result));
                    out.close();
                    con.setConnectTimeout(1000*10);
//                    BufferedReader in = new BufferedReader(
//                            new InputStreamReader(
//                                    con.getInputStream()));
//                    String decodedString;
//                    while ((decodedString = in.readLine()) != null) {
//                        System.out.println(decodedString);
//                    }
                    con.getInputStream().close();
                    System.out.println(con.getResponseCode());
                    if (con.getResponseCode() == 200)
                        JOptionPane.showMessageDialog(null, "Update successful");
                    else
                        JOptionPane.showMessageDialog(null, "Update failed");
//                    in.close();

                }
                catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(null, "Update failed");
                    System.out.println(ex.toString());
                }
            }
        });
        clearLogsButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                // reset
                list1.setModel(new DefaultListModel());
            }
        });

        tabbedPane1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                System.out.println("Tab: " + tabbedPane1.getSelectedIndex());
                if (tabbedPane1.getSelectedIndex() == 0) // tab 0
                {

                }
            }
        });

        searchButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                DefaultTableModel model = (DefaultTableModel) table1.getModel();
//                model.addRow(new Object[]{"v1", "v2"});
                model.setRowCount(0);
                String query = searchBox.getText();
                System.out.println(query);
                if (query.equals(""))
                {
                    result1.setText("(empty)");
                    return;
                }
                result1.setText(query);
                //
                try{
                    System.out.println(indexURL.getText() + "search?f=" + URLEncoder.encode(searchBox.getText(), "UTF-8"));
                    URL obj = new URL(indexURL.getText()   + "search?f=" + URLEncoder.encode(searchBox.getText(), "UTF-8"));

                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setDoOutput(false);
                    con.setDoInput(true);

                    con.setConnectTimeout(1000*10);

                    InputStream is = con.getInputStream();
                    byte[] array = new byte[is.available()];
                    is.read(array);
                    String res = new String(array);
                    is.close();
                    String[] parts = res.split("\n");
                    result1.setText(result1.getText() + " => " + String.valueOf(parts.length)  + " found.");
                    if (parts.length<=0)
                        return;
                    for (String p: parts)
                    {
                        String[] pp = p.split("/");
                        model.addRow(new Object[]{pp[0], pp[1]});
                    }

                }
                catch (Exception ex)
                {
                    System.out.println(ex.toString());
                }

            }
        });
//        updateButton.addMouseListener(new MouseAdapter() {
//            @Override
//            public void mouseClicked(MouseEvent mouseEvent) {
//                super.mouseClicked(mouseEvent);
//                DefaultTableModel model = (DefaultTableModel) localTable.getModel();
//                model.addRow(new Object[]{"v1", "v2"});
//
//            }
//        });
        startButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                // start server on port
                if (startButton.getText().equalsIgnoreCase("start"))
                {
                    // get port

                    class PingaPong implements Runnable
                    {
                        public void run()
                        {
                            try{
                                System.out.println(indexURL.getText() + "ping?" + serverPort.getText() );
                                URL obj = new URL(indexURL.getText()   + "ping?" + serverPort.getText() );

                                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                                con.setDoInput(true);
                                con.setConnectTimeout(1000*10);
                                System.out.println(con.getResponseCode());
                            }
                            catch (Exception ex)
                            {
                                System.out.println(ex.toString());
                            }
                        }
                    }

                    try{
                        executor = Executors.newScheduledThreadPool(5);
                        executor.scheduleAtFixedRate(new PingaPong(), 0, Integer.parseInt(interval.getText()), TimeUnit.SECONDS);

                        int port = Integer.parseInt(serverPort.getText());
                        String storage = storageFolder.getText();
                        if (storage.equalsIgnoreCase(""))
                        {
                            JOptionPane.showMessageDialog(null, "Empty storage");
                            return;
                        }
                        if (indexURL.getText().equalsIgnoreCase(""))
                        {
                            JOptionPane.showMessageDialog(null, "Empty Index server");
                            return;
                        }
                        Settings settings = Settings.getInstance();
                        settings.PORT = port;
                        settings.STORAGE = storage;
                        httpserver = new AppSrv().run();
                    }
                    catch (NumberFormatException ex){
                        JOptionPane.showMessageDialog(null, "Port/Interval must be a number");
                        return;
                    }
                    catch (Exception ex)
                    {
                        // pass
                        JOptionPane.showMessageDialog(null, ex.toString());
                        return;
                    }
                    startButton.setText("Stop");
                    DefaultListModel dlm =
                            (DefaultListModel) list1.getModel();
                    dlm.addElement
                            ("Server started");
                }
                else
                {
                    if (httpserver!=null)executor.shutdown();
                    if (httpserver!=null)httpserver.stop(0);
                    startButton.setText("Start");
                    DefaultListModel dlm =
                            (DefaultListModel) list1.getModel();
                    dlm.addElement
                            ("Server stopped");
                }
            }
        });
        SaveButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                // save to properties

                Properties prop = new Properties();
                OutputStream output = null;

                try {
                    output = new FileOutputStream("app.properties");
                    prop.setProperty("storageFolder", storageFolder.getText() );
                    prop.setProperty("serverPort", serverPort.getText());
                    prop.setProperty("indexURL", indexURL.getText());
                    prop.setProperty("interval", interval.getText());
                    prop.store(output, null);
                } catch (IOException io) {
                    io.printStackTrace();
                } finally {
                    if (output != null) {
                        try {
                            output.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
        });

        storageFolder.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                warn();
            }


            public void removeUpdate(DocumentEvent e) {
                warn();
            }

            public void insertUpdate(DocumentEvent e) {
                warn();
            }
            public void warn() {

                updateFolder();
            }
        });

        downloadButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                //

                int[] selection = table1.getSelectedRows();
                for (int row : selection)
                {
                    // show popup
                    Downloading.newDl(indexURL.getText(),
                            table1.getValueAt(row, 0) + "/" + table1.getValueAt(row, 1),
                            storageFolder.getText());
                }


            }
        });
        refreshButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                updateFolder(true);
            }
        });
    }
    public void updateFolder()
    {
        updateFolder(false);
    }
    public void updateFolder(boolean recalc)
    {

        textField5.setText(storageFolder.getText());
        if (storageFolder.getText().equals("")) return;
        //                  re-update folder
        File folder = new File(storageFolder.getText());
        DefaultTableModel model = (DefaultTableModel) localTable.getModel();
        model.setRowCount(0);

        if (!folder.isDirectory()) return;
        File[] listOfFiles = folder.listFiles();

        // make sub folder

        try{
            String ha = storageFolder.getText() + "/.forgetfilefast/";
            Files.createDirectories( Paths.get(ha));

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    String fn = listOfFiles[i].getName();
                    String hash;
                    if (!checkFileExist(ha + fn) || recalc)
                    {
                        hash = getSHA256(listOfFiles[i].getAbsoluteFile().toString());
                        writeFile(ha+fn, hash);
                    }
                    else
                    {
                        hash = readFile(ha+fn);
                    }
                    model.addRow(new Object[]{fn, hash});
                }
            }
        }
        catch (Exception ex)
        {
            ((DefaultListModel) list1.getModel()).addElement("Exception:" + ex.toString());
        }
    }
    private boolean checkFileExist(String f)
    {
        return new File(f).exists();
    }
    private String getSHA256(String file) {
        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-256");

            FileInputStream fis = new FileInputStream(file);
            byte[] dataBytes = new byte[1024];

            int nread = 0;
            while ((nread = fis.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            }
            return bytesToHex(md.digest());
        }
        catch (Exception ex)
        {
            ((DefaultListModel) list1.getModel()).addElement("Exception"
                    + ex.toString());
        }

        return bytesToHex(new byte[32]);
    }
    private static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte byt : bytes) result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }

    private void writeFile(String fn, String c) throws IOException
    {

        BufferedWriter writer = new BufferedWriter(new FileWriter(fn));
        writer.write(c);
        writer.close();
    }
    private static String readFile(String filePath) throws IOException
    {
        String content = "";
        content = new String ( Files.readAllBytes( Paths.get(filePath) ) );
        return content;
    }
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        JFrame fr = new JFrame("forGetfileFast");
        fr.setContentPane(new App().panel1);
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fr.setPreferredSize(new Dimension(600, 400));
        fr.pack();
        fr.setVisible(true);
    }

}

