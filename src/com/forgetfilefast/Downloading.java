package com.forgetfilefast;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class Downloading extends JDialog {
    private JPanel contentPane;
    private JButton buttonStart;
    private JButton buttonCancel;
    private JProgressBar progressBar2;
    private JButton browseButton;
    private JList list1;
    private JTextField path;
    private JTextField filename;
    private JTextField chksum;
    private JLabel status;
    private String indexURL;
    private String file;
    private String storage;
    public Downloading(String indexURL, String file, String storage){

        super((Dialog)null);
        this.indexURL = indexURL;
        this.file = file;
        this.storage = storage;
//        SwingUtilities.windowForComponent(this);
        String[] part = file.split("/");
        filename.setText(part[0]);
        if (part.length > 1) chksum.setText(part[1]);

        list1.setModel(new DefaultListModel());

        setContentPane(contentPane);
        setModal(false);
        getRootPane().setDefaultButton(buttonStart);

        buttonStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onStart();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        browseButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                JFileChooser jfc = new JFileChooser(storage);
                jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
                int returnValue = jfc.showSaveDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = jfc.getSelectedFile();
                    path.setText(selectedFile.getAbsolutePath());
                }
            }
        });
    }


    private void onStart() {
        // add your code here
        ((DefaultListModel) list1.getModel()).addElement("Start...");
        status.setText("Downloading");
        DownloadMission m = new DownloadMission(this.indexURL, this.file, this.storage, DownloadMission.DEFAULT_THREAD_COUNT, progressBar2, list1, status, this.path.getText());
        m.start();
//        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
    public static void newDl(String indexURL, String file, String storage){
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        Downloading dialog = new Downloading(indexURL, file, storage);
        dialog.setPreferredSize(new Dimension(600, 400));
        dialog.pack();
        dialog.setVisible(true);
        dialog.setAlwaysOnTop(false);
    }
    public static void main(String[] args) {

//        Downloading dialog = new Downloading();
//        dialog.pack();
//        dialog.setVisible(true);
        newDl("http://localhost:8080/", "123/dummy", "/tmp/");
//        System.exit(0);
    }

}
